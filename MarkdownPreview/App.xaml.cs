﻿using System.Windows;
using GalaSoft.MvvmLight.Threading;

namespace MarkdownPreview
{
    public partial class App
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            DispatcherHelper.Initialize();
        }
    }
}
