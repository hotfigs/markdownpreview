﻿using System;
using System.Windows;
using Newtonsoft.Json;
using System.IO;

namespace MarkdownPreview
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            var viewModel = new MainViewModel();
            viewModel.UpdateHtml += this.UpdateHtml;
            this.DataContext = viewModel;
            this.browser.Loaded += delegate {
                this.browser.Load("file:///HtmlPage.html");
            };
        }

        private void UpdateHtml(string s)
        {
            File.WriteAllText("output.txt", s);
            var serialised = JsonConvert.SerializeObject(s);
            this.browser.ExecuteScript(string.Format("loadMarkdown({0});", serialised));
            this.browser.ExecuteScript("prettyPrint();");
        }
    }
}
