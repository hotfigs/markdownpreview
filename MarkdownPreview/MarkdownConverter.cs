﻿using System.Text.RegularExpressions;

namespace MarkdownPreview
{
    public class MarkdownConverter
    {
        public string ConvertToHtml(string markdown)
        {
            //var markdownSharp = new MarkdownSharp.Markdown();
            var markdownSharp = new MarkdownDeep.Markdown();
            markdownSharp.FormatCodeBlock = FormatCodePrettyPrint;
            var result = markdownSharp.Transform(markdown);
            return result;
        }

        public static Regex rxExtractLanguage = new Regex("^({{(.+)}}[\r\n])", RegexOptions.Compiled);

        /// <summary>
        /// From http://www.toptensoftware.com/Articles/83/MarkdownDeep-Syntax-Highlighting
        /// </summary>
        public static string FormatCodePrettyPrint(MarkdownDeep.Markdown m, string code)
        {
            // Try to extract the language from the first line
            var match = rxExtractLanguage.Match(code);
            string language = null;

            if (match.Success)
            {
                // Save the language
                var g = (Group)match.Groups[2];
                language = g.ToString();

                // Remove the first line
                code = code.Substring(match.Groups[1].Length);
            }

            // If not specified, look for a link definition called "default_syntax" and
            // grab the language from its title
            if (language == null)
            {
                var d = m.GetLinkDefinition("default_syntax");
                if (d != null)
                    language = d.title;
            }

            // Common replacements
            if (language == "C#")
                language = "cs";
            if (language == "C++")
                language = "cpp";

            // Wrap code in pre/code tags and add PrettyPrint attributes if necessary
            if (string.IsNullOrEmpty(language))
                return string.Format("<pre><code>{0}</code></pre>\n", code);
            else
                return string.Format("<pre class=\"prettyprint lang-{0}\">{1}</pre>\n",
                                    language.ToLowerInvariant(), code);
        }
    }
}