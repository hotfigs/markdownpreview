﻿using System;
using System.IO;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Threading;
using Microsoft.Win32;

namespace MarkdownPreview
{
    public class MainViewModel
    {
        public MainViewModel()
        {
            this.ShowWatchFileDialog = new RelayCommand(ShowWatch_OnExecute);
            this.Html = "<body>No file loaded</body>";
        }

        public string Html { get; set; }

        public ICommand ShowWatchFileDialog { get; set; }

        public event Action<string> UpdateHtml; 

        private void LoadAndDisplayMarkdown(string path)
        {
            var markdown = File.ReadAllText(path);
            var markdownConverter = new MarkdownConverter();
            var result = markdownConverter.ConvertToHtml(markdown);
            this.Html = string.IsNullOrWhiteSpace(result) ? "<body>Could not convert to markdown</body>" : result;
            if (this.UpdateHtml != null)
            {
                DispatcherHelper.UIDispatcher.BeginInvoke((Action) (() => this.UpdateHtml(this.Html)));
            }
        }

        private void WatchFile(string path)
        {
            this.CleanupWatcher();
            var directory = Path.GetDirectoryName(path);
            var filename = Path.GetFileName(path);
            if (directory == null || filename == null) return;
            this.LoadAndDisplayMarkdown(path);
            this.CreateNewWatcher(path, directory, filename);
        }

        private void CreateNewWatcher(string path, string directory, string filename)
        {
            this.watcher = new FileSystemWatcher(directory, filename);
            this.watcher.Changed += (sender, args) => this.LoadAndDisplayMarkdown(path);
            this.watcher.EnableRaisingEvents = true;
        }

        private void CleanupWatcher()
        {
            if (this.watcher != null)
            {
                this.watcher.Dispose();
                this.watcher = null;
            }
        }

        private void ShowWatch_OnExecute()
        {
            var dialog = new OpenFileDialog();
            dialog.FileOk += (sender, args) =>
            {
                if (!dialog.CheckFileExists) return;
                this.WatchFile(dialog.FileName);
            };
            dialog.ShowDialog();
        }

        private FileSystemWatcher watcher;
    }
}